import com.hamoid.*;

VideoExport videoExport;

void setup() {
  size(500, 500);
  videoExport = new VideoExport(this);
  videoExport.startMovie();
}

void draw() {
  int rouge = (int(millis()) % 255);
  int vert = int(mouseX) % 255;
  int bleu = int(mouseY) % 255;
  color couleur_du_moment = color(rouge, vert, bleu);
  fill(couleur_du_moment);
  quad(mouseY, int(random(width)), mouseX, int(random(height)), mouseY, int(random(width)), mouseX, int(random(height)));
  videoExport.saveFrame();
}

void mouseClicked() {
  videoExport.endMovie();
  exit();
}